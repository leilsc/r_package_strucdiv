#================================================================
# Landsat NDVI data of Switzerland
#================================================================
require(raster)
require(StrucDiv2)
#require(gapfill)
#================================================================

kid <- raster("data/kidney_0015.tif")
plot(kid, colNA  ="red")
NAcount(kid)
length(unique(kid))
hist(kid)

kidmat <- matrix(kid, nrow = nrow(kid), byrow = TRUE)

# kidmat100 <- discretizeImage(kidmat, n_grey = 100)
# kid100 <- raster(kidmat100)
# extent(kid100) <- extent(kid)
# crs(kid100) <- crs(kid)
# plot(kid100)
# NAcount(kid100)
# length(unique(kid100))
# writeRaster(kid100, "data/Landsat_ndkid240m_100GL.tif", overwrite = TRUE)

# kid10 <- bin(kid, 12) #  equal sized bins
# length(unique(kid10))
# plot(kid10)

kidmat10 <- discretizeImage(kidmat, n_grey = 10)
kid10 <- raster(kidmat10)
extent(kid10) <- extent(kid)
plot(kid10)
NAcount(kid10)
length(unique(kid10))
writeRaster(kid10, "data/kidney_10GL.tif", overwrite = TRUE)

#================================================================

